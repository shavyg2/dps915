 // Linear Algebra - Workshop 2
 // w2.custom.cpp

 #include <iostream>
 #include <ctime>
 #include <chrono>
 #include <cstdlib>
 using namespace std::chrono;

 void init(float* a, int n) {
     const float randf = 1.0f / (float) RAND_MAX; 
     for (int i = 0; i < n; i++)
         a[i] = std::rand() * randf;
 }

 void reportTime(const char* msg, steady_clock::duration span) {
     auto ms = duration_cast<milliseconds>(span);
     std::cout << msg << " - took - " <<
     ms.count() << " millisecs" << std::endl;
 }

 float sdot(int n, const float* a, const float* b) {
    float result=0;

    for(int i =0;i<n;i++){
      result+= a[i] * b[i];
    }
    return result;
 }

 void sgemv(const float* a, int n, const float* v, float* w) {

    // insert your custom code here
    //a is a matrix while v is a vector
    //results go into w
    
    //std::cout << "The size of n is " <<  n << std::endl;    
    //display
    int i;
    // for(int z =0;z < n;z++){
    //   for(int y=0; y < n; y++){
    //     std::cout << a[z+y] << " ";
    //   }
    //   std::cout << std::endl;
    // }

    // std::cout << std::endl;

    // for(int x=0;x<n;x++){
    //   std::cout << v[x] <<std::endl;
    // }

    //calculation
    for(i=0; i< n; i++){
      w[i]=0;
      for(int j=0; j<n; j++){
        w[i] += a[i+j] * v[j];
      }
    }

    // std::cout << std::endl;

    //display
    // for(i=0; i< n;i++){
    //   std::cout << w[i] << std::endl;
    // }
 }

 void sgemm(const float* a, const float* b, int n, float* c) {

   // insert your custom code here
   //Now we have 2 matrix to find the dot product
   //std::cout << std::endl;
   int row = 0;
   int col = 0;
   int count = 0;
   for(row=0;row<n;row++){
     for(col=0;col<n;col++){
       c[row+col]=0;
       std::cout << "c[" << row << "," <<col << "]" << "=";
       for(int offset = 0;offset<n;offset++){
         std::cout << "a["<<row <<"," << offset << "]";
         std::cout << "b["<<offset <<"," << col << "] ";
         c[row+col]+= a[row*n+col] * b[row+col*n];
         count++;
       }
       std::cout << std::endl;
     }
   } 
 }

void displayM(float* matrix,int n){
  for(int i=0;i<n;i++){
    for(int j=0;j<n;j++){
      std::cout << matrix[i+j*n] << " ";
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
}


 int main(int argc, char** argv) {

     // interpret command-line argument
     if (argc != 2) {
         std::cerr << argv[0] << ": invalid number of arguments\n"; 
         std::cerr << "Usage: " << argv[0] << "  size_of_matrices\n"; 
         return 1;
     }
     int n = std::atoi(argv[1]);
     steady_clock::time_point ts, te;
     
     //Vectors
     float* v = new float[n];
     float* w = new float[n];
     
     //Matrices
     float* a = new float[n * n];
     float* b = new float[n * n];
     float* c = new float[n * n];

     // initialization
     std::srand(std::time(nullptr));
     ts = steady_clock::now();
     init(a, n * n);
     init(b, n * n);
     init(v, n);
     init(w, n);
     te = steady_clock::now();
     reportTime("initialization         ", te - ts); 

     // vector-vector - dot product of v and w
     ts = steady_clock::now();
     //float sdot_result;
     // sdot_result=sdot(n, v, w);
     te = steady_clock::now();  
     //std::cout << "Sdot result is " << sdot_result << std::endl;

     reportTime("vector-vector operation", te - ts); 

     // matrix-vector - product of a and v
     ts = steady_clock::now();
     sgemv(a, n, v, w);
     te = steady_clock::now();
     reportTime("matrix-vector operation", te - ts); 
     


     // matrix-matrix - product of a and b
     ts = steady_clock::now();
     sgemm(a, b, n, c);

     displayM(a,n);
     displayM(b,n);
     displayM(c,n);    

     te = steady_clock::now();
     reportTime("matrix-matrix operation", te - ts); 

     delete [] v;
     delete [] w;
     delete [] a;
     delete [] b;
     delete [] c;
 }
